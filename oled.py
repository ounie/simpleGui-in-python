import multiprocessing
import os
import signal
import threading
from ctypes import *
from exchange.button import *
from exchange.log import log_factory
from hmi.engine import engine_init
from hmi.gui.apilib import HMIENGINE_LIB
from hmi.gui.hmiengine import RTC_EVENT, HMI_EVENT_BASE, EVENT_TYPE, EVENT_ID, KEY_PRESS_EVENT
from hmi.prepare import gui_prepare

gui_trigger_event = threading.Event()
colon_flag = 0
rtc_flag = 0


def key_press_process(key_code):
    key_event = KEY_PRESS_EVENT(
        HMI_EVENT_BASE(EVENT_TYPE.EVENT_TYPE_ACTION, EVENT_ID.EVENT_ID_KEY_PRESS, sizeof(KEY_PRESS_EVENT)), key_code)
    HMIENGINE_LIB.HMI_ProcessEvent(cast(pointer(key_event), POINTER(HMI_EVENT_BASE)))


def rtc_process(process):
    HMIENGINE_LIB.HMI_ProcessEvent(cast(pointer(process), POINTER(HMI_EVENT_BASE)))


def rtc_event_sighandler(signum, frame):
    global colon_flag, rtc_flag
    colon_flag = 0 if colon_flag else 1
    rtc_flag = 1
    gui_trigger_event.set()


def oled_process(instrument_data_status: multiprocessing.Value):
    global rtc_flag
    gui_prepare()
    engine_init()
    signal.signal(signal.SIGALRM, rtc_event_sighandler)
    signal.setitimer(signal.ITIMER_REAL, 1, 1)

    button = ButtonValue()

    logger = log_factory('/mnt/log/log.txt')
    rotary_key_thread = ButtonRotaryThread('/dev/input/event0', gui_trigger_event, logger, button)
    rotary_key_thread.start()

    while True:
        gui_trigger_event.wait()
        gui_trigger_event.clear()
        if button.value is not None:
            key_press_process(button.value)
            button.value = None

        if rtc_flag:
            rtc_flag = 0
            rtc_event_obj = RTC_EVENT(HMI_EVENT_BASE(EVENT_TYPE.EVENT_TYPE_DATA, EVENT_ID.EVENT_ID_RTC,
                                                 sizeof(RTC_EVENT)), colon_flag, instrument_data_status.value)
            rtc_process(rtc_event_obj)


if __name__ == '__main__':
    oled_process(multiprocessing.Value('b', 1))
