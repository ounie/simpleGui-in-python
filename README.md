# **SimpleGUI-in-Python**

**感谢SimpleGUI这个非常棒的开源项目：**

https://gitee.com/Polarix/simplegui

### 项目情况

- 目前只针对list、menu、scrollbar三种控件做了bindings，因为做菜单，主要使用这三类控件。
- 使用的python内置的ctypes
- 项目无外部依赖。目前使用Python 3.8.5进行的测试。

### 适配说明

1. hmi/gui/constant.py文件中定义了各种常量。

- 屏幕宽度和高度
- 键值
- 输入设备路径
- 画面id标识

2. 程序需要加载的so文件，可以拷贝到系统链接目录下，或者更新环境变量，将库加载地址指向so所在路径