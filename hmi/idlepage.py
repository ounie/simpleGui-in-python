import os
import sys

import time
from ctypes import *

from hmi.prepare import lcd_clear_func
from .gui.apilib import GUI_LIB, HMIENGINE_LIB
from .gui.constant import HMI_SCREEN_ID_FIRST_LEVEL_MENU, KEY_OK
from .gui.font import FONT_8
from .gui.hmiengine import HMI_EVENT_BASE, EVENT_ID, RTC_EVENT, KEY_PRESS_EVENT
from .gui.textbase import TextBase
from .gui.typedef import SGUI_SCR_DEV, SGUI_RECT, SGUI_POINT, SGUI_BMP_RES


def calc_space_left():
    data_partition = os.statvfs('/mnt/app')
    used_space_rate = round(((data_partition.f_blocks - data_partition.f_bfree) / data_partition.f_blocks * 100))
    return used_space_rate


icon = SGUI_BMP_RES(5, 3, b'\x04\x06\x07\x06\x04')
icon1 = SGUI_BMP_RES(5, 3, b'\x00\x00\x00\x00\x00')
icon_flag = 0


def refresh_rtc(device_if: SGUI_SCR_DEV, colon):
    time_struct = time.localtime(round(time.time()))
    time_str_format = '%Y-%m-%d %H:%M' if colon else '%Y-%m-%d %H %M'
    time_display = time.strftime(time_str_format, time_struct)

    GUI_LIB.SGUI_Text_DrawText(device_if, str.encode(time_display), FONT_8, SGUI_RECT(30, 1, 98, 13), SGUI_POINT(0, 0),
                               0)

    # global icon_flag
    # stIconArea = SGUI_RECT(10, 10, 5, 3)
    # stIconInnerPos = SGUI_POINT(0, 0)
    #
    # icon_flag = 0 if icon_flag else 1

    # GUI_LIB.SGUI_Basic_DrawBitMap(device_if, stIconArea, stIconInnerPos, icon, 0)


def refresh_instrument_status(device_if: SGUI_SCR_DEV, status):
    stIconArea = SGUI_RECT(40, 16, 5, 3)
    stIconInnerPos = SGUI_POINT(0, 0)
    GUI_LIB.SGUI_Basic_DrawBitMap(device_if, stIconArea, stIconInnerPos, icon if status else icon1, 0)


def show_left_space(device_if: SGUI_SCR_DEV, path: str):
    data_partition = os.statvfs(path)
    used_space_rate = round(((data_partition.f_blocks - data_partition.f_bfree) / data_partition.f_blocks * 100))

    GUI_LIB.SGUI_Text_DrawText(device_if, str.encode(("%d" % used_space_rate) + '%'), FONT_8, SGUI_RECT(1, 1, 20, 13),
                               SGUI_POINT(0, 0),
                               0)


port_layout = {
    'P1': SGUI_RECT(1, 14, 64, 13),
    'P2': SGUI_RECT(65, 14, 64, 13),
    'P3': SGUI_RECT(1, 28, 64, 13),
    'P4': SGUI_RECT(65, 28, 64, 13),
}


def show_port_status(device_if: SGUI_SCR_DEV, name, status):
    GUI_LIB.SGUI_Text_DrawText(device_if, str.encode(name + ':' + status), FONT_8, port_layout[name], SGUI_POINT(0, 0),
                               0)


class IdlePage(TextBase):
    def __init__(self, menu_id):
        super().__init__(menu_id)

    def HMI_RefreshScreen(self, device_if: SGUI_SCR_DEV, pstParameters):
        refresh_rtc(device_if, pstParameters)
        refresh_instrument_status(device_if, pstParameters)
        show_left_space(device_if, '/mnt/app')
        show_port_status(device_if, 'P1', 'on')
        show_port_status(device_if, 'P2', 'off')
        show_port_status(device_if, 'P3', 'off')
        show_port_status(device_if, 'P4', 'off')
        return 1

    def HMI_Prepare(self, device_if: SGUI_SCR_DEV, pstParameters):
        lcd_clear_func()
        self.HMI_RefreshScreen(device_if, pstParameters)

        return 1

    def HMI_ProcessEvent(self, device_if: SGUI_SCR_DEV, event: HMI_EVENT_BASE, piActionID):
        event_contents = event.contents
        id = event_contents.iID
        if id == EVENT_ID.EVENT_ID_RTC:
            rtc_event = cast(event, POINTER(RTC_EVENT))
            refresh_rtc(device_if, rtc_event.contents.flag)
            refresh_instrument_status(device_if, rtc_event.contents.instrument_recv_status)
        elif id == EVENT_ID.EVENT_ID_KEY_PRESS:
            key_event = cast(event, POINTER(KEY_PRESS_EVENT))
            key_value = key_event.contents.value
            if key_value == KEY_OK:
                HMIENGINE_LIB.HMI_SwitchScreen(HMI_SCREEN_ID_FIRST_LEVEL_MENU, 1)

        return 1
