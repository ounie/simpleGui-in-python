from ctypes import *

from .deviceinfomenu import DeviceInfoMenu, DEVICE_INFO_MENU_ITEMS
from .gui.apilib import HMIENGINE_LIB
from .gui.constant import HMI_SCREEN_ID_IDLE, HMI_SCREEN_ID_FIRST_LEVEL_MENU, HMI_SCREEN_ID_DEVICE_INFO_MENU
from .gui.hmiengine import HMI_SCREEN_OBJECT, HMI_EVENT_BASE
from hmi.prepare import DEVICE_IF
from .gui.typedef import SGUI_SCR_DEV
from .idlepage import IdlePage
from .firstlevelmenu import FIRST_MENU_ITEMS, FirstMenu

g_stHMI_IdlePage = IdlePage(HMI_SCREEN_ID_IDLE)
g_stHMI_FirstLevelMenuPage = FirstMenu(HMI_SCREEN_ID_FIRST_LEVEL_MENU, FIRST_MENU_ITEMS)
g_stHMI_DeviceInfoMenuPage = DeviceInfoMenu(HMI_SCREEN_ID_DEVICE_INFO_MENU, DEVICE_INFO_MENU_ITEMS)

cur_screen_obj = pointer(g_stHMI_IdlePage.hmi_page_obj)
arrayScreen = [
                pointer(g_stHMI_IdlePage.hmi_page_obj),
                pointer(g_stHMI_FirstLevelMenuPage.hmi_page_obj),
                pointer(g_stHMI_DeviceInfoMenuPage.hmi_page_obj)
               ]
g_arrpstScreenObjs = POINTER(HMI_SCREEN_OBJECT) * len(arrayScreen)
g_arrayScreen = g_arrpstScreenObjs(*arrayScreen)


class HMI_ENGINE_OBJECT(Structure):
    pass


HMI_ENGINE_OBJECT._fields_ = [
    ('ScreenObjPtr', POINTER(POINTER(HMI_SCREEN_OBJECT) * len(arrayScreen))),
    ('ScreenCount', c_int),
    ('CurrentScreenObject', POINTER(HMI_SCREEN_OBJECT)),
    ('Interface', POINTER(SGUI_SCR_DEV))
]

g_stEngine = HMI_ENGINE_OBJECT(pointer(g_arrayScreen), len(arrayScreen), cur_screen_obj, pointer(DEVICE_IF))


def hmiengine_prepare():
    HMIENGINE_LIB.HMI_ActiveEngine.argtypes = (POINTER(HMI_ENGINE_OBJECT), c_int)
    HMIENGINE_LIB.HMI_ActiveEngine.restype = c_int

    HMIENGINE_LIB.HMI_StartEngine.argtypes = (c_void_p,)
    HMIENGINE_LIB.HMI_StartEngine.restype = c_int

    HMIENGINE_LIB.HMI_ProcessEvent.argtypes = (POINTER(HMI_EVENT_BASE),)
    HMIENGINE_LIB.HMI_ProcessEvent.restype = c_int

    HMIENGINE_LIB.HMI_SwitchScreen.argtypes = (c_int, c_void_p)
    HMIENGINE_LIB.HMI_SwitchScreen.restype = c_int

    HMIENGINE_LIB.HMI_GoBack.argtypes = (c_void_p,)
    HMIENGINE_LIB.HMI_GoBack.restype = c_int


def page_init():
    g_stHMI_FirstLevelMenuPage.init(DEVICE_IF)
    g_stHMI_DeviceInfoMenuPage.init(DEVICE_IF)
    pass


def engine_init():
    hmiengine_prepare()
    page_init()
    HMIENGINE_LIB.HMI_ActiveEngine(g_stEngine, HMI_SCREEN_ID_IDLE)
    HMIENGINE_LIB.HMI_StartEngine(None)
