from ctypes import *

from .apilib import GUI_LIB
from .typedef import SGUI_SCR_DEV, SGUI_FONT_RES, SGUI_RECT, SGUI_POINT

# t = c_char_p(b"hello")
# GUI_LIB.SGUI_Text_DrawText(DEVICE_IF, t, FONT_8, SGUI_RECT(30, 1, 50, 13), SGUI_POINT(0, 0), 0)


def text_prepare():
    GUI_LIB.SGUI_Text_DrawText.argtypes = (POINTER(SGUI_SCR_DEV), c_char_p, POINTER(SGUI_FONT_RES),
                                           POINTER(SGUI_RECT), POINTER(SGUI_POINT), c_int)
    GUI_LIB.SGUI_Text_DrawText.restype = None