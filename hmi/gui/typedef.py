import ctypes
import enum
from ctypes import Structure


class SGUI_RECT(Structure):
    _fields_ = [
        ('x', ctypes.c_int),
        ('y', ctypes.c_int),
        ('width', ctypes.c_int),
        ('height', ctypes.c_int)
    ]


class SGUI_AREA_SIZE(Structure):
    _fields_ = [
        ('width', ctypes.c_int),
        ('height', ctypes.c_int)
    ]


class SGUI_POINT(Structure):
    _fields_ = [
        ('x', ctypes.c_int),
        ('y', ctypes.c_int)
    ]


class SGUI_BUFFER(Structure):
    _fields_ = [
        ('buffer', ctypes.c_char_p),
        ('size', ctypes.c_size_t)
    ]


class SGUI_COLOR(enum.IntEnum):
    SGUI_COLOR_TRANS = -1
    SGUI_COLOR_BKGCLR = 0
    SGUI_COLOR_FRGCLR = 1


class SGUI_DRAW_MODE(enum.IntEnum):
    SGUI_DRAW_NORMAL = 0
    SGUI_DRAW_REVERSE = 1


class SGUI_SCR_DEV(Structure):
    _fields_ = [
        ('stSize', SGUI_AREA_SIZE),
        ('stBuffer', SGUI_BUFFER),
        ('fnInitialize', ctypes.c_void_p),
        ('fnClear', ctypes.c_void_p),
        ('fnSetPixel', ctypes.c_void_p),
        ('fnFillRect', ctypes.c_void_p),
        ('fnSyncBuffer', ctypes.c_void_p)
    ]


class SGUI_FONT_RES(Structure):
    _fields_ = [
        ('iHalfWidth', ctypes.c_int),
        ('iFullWidth', ctypes.c_int),
        ('iHeight', ctypes.c_int),
        ('fnGetIndex', ctypes.c_void_p),
        ('fnGetData', ctypes.c_void_p),
        ('fnStepNext', ctypes.c_void_p),
        ('fnIsFullWidth', ctypes.c_void_p)
    ]


class SGUI_BMP_RES(Structure):
    _fields_ = [
        ('iWidth', ctypes.c_int),
        ('iHeight', ctypes.c_int),
        ('pData', ctypes.c_char_p)
    ]
