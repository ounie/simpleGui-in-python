import enum
from ctypes import *

from .apilib import HMIENGINE_LIB
from .typedef import SGUI_SCR_DEV


class HMI_ENGINE_RESULT(enum.IntEnum):
    HMI_RET_ERROR = -3
    HMI_RET_INVALID_DATA = -2
    HMI_RET_ERROR_STATE = -1
    HMI_RET_ABNORMAL = 0
    HMI_RET_NORMAL = 1


class EVENT_TYPE(enum.IntEnum):
    EVENT_TYPE_ANY = 0
    EVENT_TYPE_ACTION = 1
    EVENT_TYPE_DATA = 2


class PROC_ACTION_ID(enum.IntEnum):
    HMI_PROC_NO_ACT = 0
    HMI_PROC_CONFIRM = 1
    HMI_PROC_CANCEL = 2


class EVENT_ID(enum.IntEnum):
    EVENT_ID_UNKNOW = 0
    EVENT_ID_KEY_PRESS = 1
    EVENT_ID_TIMER = 2
    EVENT_ID_RTC = 3
    EVENT_ID_MAX = 99


class HMI_EVENT_BASE(Structure):
    _fields_ = [
        ('iType', c_int),
        ('iID', c_int),
        ('iSize', c_int)
    ]


class HMI_SCREEN_ACTION(Structure):
    _fields_ = [
        ('Initialize', c_void_p),
        ('Prepare', c_void_p),
        ('Repaint', c_void_p),
        ('ProcessEvent', c_void_p),
        ('PostProcess', c_void_p)
    ]


class HMI_SCREEN_OBJECT(Structure):
    pass


HMI_SCREEN_OBJECT._fields_ = [
    ('iScreenID', c_int),
    ('pstActions', POINTER(HMI_SCREEN_ACTION)),
    ('pstPrevious', c_void_p)
]


# class HMI_ENGINE_OBJECT(Structure):
#     _fields_ = [
#         ('ScreenObjPtr', POINTER(POINTER(HMI_SCREEN_OBJECT) * 2)),
#         ('ScreenCount', c_int),
#         ('CurrentScreenObject', POINTER(HMI_SCREEN_OBJECT)),
#         ('Interface', POINTER(SGUI_SCR_DEV))
#     ]


class KEY_PRESS_EVENT(Structure):
    _fields_ = [
        ('Head', HMI_EVENT_BASE),
        ('value', c_uint16)
    ]


class RTC_EVENT(Structure):
    _fields_ = [
        ('Head', HMI_EVENT_BASE),
        ('flag', c_int),
        ('instrument_recv_status', c_int)
    ]

