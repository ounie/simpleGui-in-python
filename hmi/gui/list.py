from .apilib import GUI_LIB
from .itemsbase import SGUI_ITEMS_BASE, SGUI_ITEMS_ITEM
from ctypes import *

from .scrollbar import SGUI_SCROLLBAR_STRUCT
from .typedef import SGUI_RECT, SGUI_FONT_RES, SGUI_SCR_DEV


class SGUI_LIST(Structure):
    _fields_ = [
        ('stLayout', SGUI_RECT),
        ('stItems', SGUI_ITEMS_BASE),
        ('szTitle', c_char_p),
        ('stScrollBar', SGUI_SCROLLBAR_STRUCT),
        ('pstFontRes', POINTER(SGUI_FONT_RES))
    ]


def list_prepare():
    GUI_LIB.SGUI_List_Initialize.argtypes = (POINTER(SGUI_LIST), POINTER(SGUI_RECT), POINTER(SGUI_FONT_RES),
                                             c_char_p, POINTER(SGUI_ITEMS_ITEM), c_int)
    GUI_LIB.SGUI_List_Initialize.restype = None

    GUI_LIB.SGUI_List_Repaint.argtypes = (POINTER(SGUI_SCR_DEV), POINTER(SGUI_LIST))
    GUI_LIB.SGUI_List_Repaint.restype = None