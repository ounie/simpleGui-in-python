from ctypes import *

from .apilib import GUI_LIB
from .typedef import SGUI_SCR_DEV, SGUI_BMP_RES, SGUI_RECT, SGUI_POINT


def basic_prepare():
    GUI_LIB.SGUI_Basic_ClearScreen.argtypes = (POINTER(SGUI_SCR_DEV),)
    GUI_LIB.SGUI_Basic_ClearScreen.restype = None
    GUI_LIB.SGUI_Basic_DrawPoint.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawPoint.restype = None
    GUI_LIB.SGUI_Basic_DrawLine.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int, c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawLine.restype = None
    GUI_LIB.SGUI_Basic_DrawHorizontalLine.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawHorizontalLine.restype = None
    GUI_LIB.SGUI_Basic_DrawVerticalLine.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawVerticalLine.restype = None
    GUI_LIB.SGUI_Basic_DrawRectangle.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int, c_int,c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawRectangle.restype = None
    GUI_LIB.SGUI_Basic_DrawBitMap.argtypes = (POINTER(SGUI_SCR_DEV), POINTER(SGUI_RECT), POINTER(SGUI_POINT),
                                              POINTER(SGUI_BMP_RES), c_int)
    GUI_LIB.SGUI_Basic_DrawBitMap.restype = None
    GUI_LIB.SGUI_Basic_DrawCircle.argtypes = (POINTER(SGUI_SCR_DEV), c_int, c_int, c_int, c_int, c_int)
    GUI_LIB.SGUI_Basic_DrawCircle.restype = None