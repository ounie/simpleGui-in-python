from .typedef import SGUI_SCR_DEV
from .hmiengine import HMI_EVENT_BASE, HMI_SCREEN_ACTION, HMI_SCREEN_OBJECT
from ctypes import *


class TextBase:

    def __init__(self, menu_id):
        self.menu_id = menu_id

    def HMI_Initialize(self, device_if: SGUI_SCR_DEV):
        return 1

    def HMI_RefreshScreen(self, device_if: SGUI_SCR_DEV, pstParameters):
        return 1

    def HMI_Prepare(self, device_if: SGUI_SCR_DEV, pstParameters):
        return 1

    def HMI_ProcessEvent(self, device_if: SGUI_SCR_DEV, event: HMI_EVENT_BASE, piActionID):
        return 1

    def HMI_PostProcess(self, device_if: SGUI_SCR_DEV, eProcResult, iActionID):
        return 1

    @property
    def init(self):
        return CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV))(self.HMI_Initialize)

    @property
    def hmi_page_obj(self):
        HMI_Prepare_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_void_p)(self.HMI_Prepare)
        HMI_RefreshScreen_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_void_p)(self.HMI_RefreshScreen)
        HMI_ProcessEvent_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), POINTER(HMI_EVENT_BASE), POINTER(c_int))(
            self.HMI_ProcessEvent)
        HMI_PostProcess_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_int, c_int)(self.HMI_PostProcess)

        s_stActions = HMI_SCREEN_ACTION(None, cast(HMI_Prepare_cb, c_void_p),
                                            cast(HMI_RefreshScreen_cb, c_void_p),
                                            cast(HMI_ProcessEvent_cb, c_void_p),
                                            cast(HMI_PostProcess_cb, c_void_p))

        return HMI_SCREEN_OBJECT(self.menu_id, pointer(s_stActions), None)
