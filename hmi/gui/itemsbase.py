from ctypes import *

from .apilib import GUI_LIB
from .typedef import SGUI_RECT, SGUI_FONT_RES


class SGUI_ITEMS_ITEM(Structure):
    pass


SGUI_ITEMS_ITEM._fields_ = [
    ('cszLabelText', c_char_p),
    ('szVariableText', c_char_p),
    ('pstPrev', POINTER(SGUI_ITEMS_ITEM)),
    ('pstNext', POINTER(SGUI_ITEMS_ITEM))
]


class SGUI_ITEM_SELECTION(Structure):
    _fields_ = [
        ('iIndex', c_int),
        ('pstItem', POINTER(SGUI_ITEMS_ITEM))
    ]


class SGUI_ITEMS_BASE(Structure):
    _fields_ = [
        ('stLayout', SGUI_RECT),
        ('pstFirstItem', POINTER(SGUI_ITEMS_ITEM)),
        ('pstLastItem', POINTER(SGUI_ITEMS_ITEM)),
        ('iCount', c_int),
        ('stSelection', SGUI_ITEM_SELECTION),
        ('stVisibleStart', SGUI_ITEM_SELECTION),
        ('stVisibleEnd', SGUI_ITEM_SELECTION),
        ('iVisibleItems', c_int),
        ('iItemPaintOffset', c_int),
        ('pstFontRes', POINTER(SGUI_FONT_RES))
    ]


def itembase_prepare():
    GUI_LIB.SGUI_ItemsBase_Selecte.argtypes = (POINTER(SGUI_ITEMS_BASE), c_int)
    GUI_LIB.SGUI_ItemsBase_Selecte.restype = None