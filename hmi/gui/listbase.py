from ctypes import *

from .apilib import GUI_LIB
from .constant import KEY_OK, KEY_LEFT, KEY_RIGHT
from .font import FONT_8
from .hmiengine import HMI_EVENT_BASE, HMI_SCREEN_OBJECT, HMI_SCREEN_ACTION
from .itemsbase import SGUI_ITEMS_ITEM
from .list import SGUI_LIST
from .typedef import SGUI_SCR_DEV, SGUI_RECT


class ListBase:
    def __init__(self, menu_id, items: list):
        self.items_text = []
        self.s_arrstListItems = []
        for t in items:
            self.items_text.append(t)
            self.s_arrstListItems.append(SGUI_ITEMS_ITEM(str.encode(t), None, None, None))
        self.s_stMenuListObject = SGUI_LIST()
        self.s_arrstListItemsType = SGUI_ITEMS_ITEM * len(self.s_arrstListItems)
        self.p_arrstListItems = self.s_arrstListItemsType(*self.s_arrstListItems)
        self._g_stHMI_Page = None
        self.menu_id = menu_id
        self._key_dispatch = {
            KEY_OK: self.ok_process,
            KEY_LEFT: self.left_process,
            KEY_RIGHT: self.right_process
        }

    @property
    def key_dispatch(self):
        return self._key_dispatch

    def ok_process(self, device_if: SGUI_SCR_DEV):
        pass

    def left_process(self, device_if: SGUI_SCR_DEV):
        cur_index = self.s_stMenuListObject.stItems.stSelection.iIndex
        if cur_index:
            now_index = cur_index - 1
            GUI_LIB.SGUI_ItemsBase_Selecte(self.s_stMenuListObject.stItems, now_index)
            GUI_LIB.SGUI_List_Repaint(device_if, self.s_stMenuListObject)

    def right_process(self, device_if: SGUI_SCR_DEV):
        cur_index = self.s_stMenuListObject.stItems.stSelection.iIndex
        bottom_index = self.s_stMenuListObject.stItems.iCount - 1
        if cur_index < bottom_index:
            GUI_LIB.SGUI_ItemsBase_Selecte(self.s_stMenuListObject.stItems, cur_index + 1)
            GUI_LIB.SGUI_List_Repaint(device_if, self.s_stMenuListObject)

    def HMI_Initialize(self, device_if: SGUI_SCR_DEV):
        stListLayout = SGUI_RECT(0, 0, device_if.contents.stSize.width, device_if.contents.stSize.height)

        GUI_LIB.SGUI_List_Initialize(self.s_stMenuListObject, stListLayout, FONT_8, None,
                                     cast(self.p_arrstListItems, POINTER(SGUI_ITEMS_ITEM)), len(self.s_arrstListItems))

        return 1

    def HMI_RefreshScreen(self, device_if: SGUI_SCR_DEV, pstParameters):
        GUI_LIB.SGUI_List_Repaint(device_if, self.s_stMenuListObject)
        return 1

    def HMI_Prepare(self, device_if: SGUI_SCR_DEV, pstParameters):
        return 1

    def HMI_ProcessEvent(self, device_if: SGUI_SCR_DEV, event: HMI_EVENT_BASE, piActionID):
        return 1

    def HMI_PostProcess(self, device_if: SGUI_SCR_DEV, eProcResult, iActionID):
        return 1

    @property
    def init(self):
        return CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV))(self.HMI_Initialize)

    @property
    def hmi_page_obj(self):
        HMI_Initialize_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV))(self.HMI_Initialize)
        HMI_Prepare_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_void_p)(self.HMI_Prepare)
        HMI_RefreshScreen_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_void_p)(self.HMI_RefreshScreen)
        HMI_ProcessEvent_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), POINTER(HMI_EVENT_BASE), POINTER(c_int))(self.HMI_ProcessEvent)
        HMI_PostProcess_cb = CFUNCTYPE(c_int, POINTER(SGUI_SCR_DEV), c_int, c_int)(self.HMI_PostProcess)

        s_stActions = HMI_SCREEN_ACTION(cast(HMI_Initialize_cb, c_void_p), cast(HMI_Prepare_cb, c_void_p),
                                            cast(HMI_RefreshScreen_cb, c_void_p),
                                            cast(HMI_ProcessEvent_cb, c_void_p),
                                            cast(HMI_PostProcess_cb, c_void_p))

        return HMI_SCREEN_OBJECT(self.menu_id, pointer(s_stActions), None)
