from ctypes import *

from .apilib import GUI_LIB
from .itemsbase import SGUI_ITEMS_BASE, SGUI_ITEMS_ITEM
from .list import SGUI_LIST
from .scrollbar import SGUI_SCROLLBAR_STRUCT
from .typedef import SGUI_RECT, SGUI_FONT_RES, SGUI_SCR_DEV


class SGUI_MENU(Structure):
    _fields_ = [
        ('stLayout', SGUI_RECT),
        ('stItems', SGUI_ITEMS_BASE),
        ('szTitle', c_char_p),
        ('stScrollBar', SGUI_SCROLLBAR_STRUCT),
        ('pstFontRes', POINTER(SGUI_FONT_RES))
    ]


def menu_prepare():
    GUI_LIB.SGUI_Menu_Initialize.argtypes = (POINTER(SGUI_LIST), POINTER(SGUI_RECT), POINTER(SGUI_FONT_RES),
                                             POINTER(SGUI_ITEMS_ITEM), c_int)
    GUI_LIB.SGUI_Menu_Initialize.restype = None

    GUI_LIB.SGUI_Menu_Repaint.argtypes = (POINTER(SGUI_SCR_DEV), POINTER(SGUI_MENU))
    GUI_LIB.SGUI_Menu_Repaint.restype = None