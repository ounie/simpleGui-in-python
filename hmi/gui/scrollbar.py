import enum
from ctypes import *

from .typedef import SGUI_RECT


class SGUI_SCROLLBAR_DIRECTION(enum.IntEnum):
    SGUI_SCROLLBAR_VERTICAL = 0
    SGUI_SCROLLBAR_HORIZONTAL = 1


class SGUI_SCROLLBAR_PARAM(Structure):
    _fields_ = [
        ('stLayout', SGUI_RECT),
        ('sMaxValue', c_size_t),
        ('eDirection', c_int)
    ]


class SGUI_SCROLLBAR_DATA(Structure):
    _fields_ = [
        ('sValue', c_size_t)
    ]


class SGUI_SCROLLBAR_STRUCT(Structure):
    _fields_ = [
        ('stParam', SGUI_SCROLLBAR_PARAM),
        ('stData', SGUI_SCROLLBAR_DATA)
    ]