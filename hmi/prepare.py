from .gui.basic import basic_prepare
from .gui.constant import SCREEN_WIDTH, SCREEN_HEIGHT
from .gui.fb_init import *
from ctypes import *

from .gui.itemsbase import itembase_prepare
from .gui.list import list_prepare
from .gui.menu import menu_prepare
from .gui.text import text_prepare
from .gui.typedef import SGUI_SCR_DEV

init_lcd_func = CFUNCTYPE(c_void_p)(init_lcd)
lcd_fresh_func = CFUNCTYPE(c_void_p)(lcd_refresh)
draw_pixel_sgui_func = CFUNCTYPE(c_void_p, c_int, c_int, c_uint)(draw_pixel_sgui)
lcd_clear_func = CFUNCTYPE(c_void_p)(lcd_clear)
bmp_buffer = create_string_buffer(b'', 512)

DEVICE_IF = SGUI_SCR_DEV((SCREEN_WIDTH, SCREEN_HEIGHT), (cast(bmp_buffer, c_char_p), 512),
                         cast(init_lcd_func, c_void_p), cast(lcd_clear_func, c_void_p),
                         cast(draw_pixel_sgui_func, c_void_p), None, cast(lcd_fresh_func, c_void_p))


def gui_prepare():
    basic_prepare()
    list_prepare()
    menu_prepare()
    text_prepare()
    itembase_prepare()
