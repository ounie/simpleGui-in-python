from ctypes import *

from .gui.apilib import HMIENGINE_LIB, GUI_LIB
from .gui.constant import HMI_SCREEN_ID_DEVICE_INFO_MENU
from .gui.hmiengine import HMI_EVENT_BASE, EVENT_ID, KEY_PRESS_EVENT, EVENT_TYPE
from .gui.listbase import ListBase
from .gui.typedef import SGUI_SCR_DEV

DEVICE_INFO_MENU_ITEMS = ['ID.', 'SN.', 'Firmware ', 'IP:', '', 'GW:', '< exit']


class DeviceInfoMenu(ListBase):
    def __init__(self, menu_id, items: list):
        super().__init__(menu_id, items)

    def HMI_Prepare(self, device_if: SGUI_SCR_DEV, pstParameters):
        GUI_LIB.SGUI_ItemsBase_Selecte(self.s_stMenuListObject.stItems, 0)
        self.HMI_RefreshScreen(device_if, pstParameters)

        return 1

    def HMI_ProcessEvent(self, device_if: SGUI_SCR_DEV, event: HMI_EVENT_BASE, piActionID):
        event_contents = event.contents
        event_type = event_contents.iType
        event_id = event_contents.iID
        if event_type == EVENT_TYPE.EVENT_TYPE_ACTION and event_id == EVENT_ID.EVENT_ID_KEY_PRESS:
            key_event = cast(event, POINTER(KEY_PRESS_EVENT))
            key_value = key_event.contents.value
            self.key_dispatch[key_value](device_if)

        return 1

    def ok_process(self, device_if: SGUI_SCR_DEV):
        cur_index = self.s_stMenuListObject.stItems.stSelection.iIndex
        if cur_index == 6:
            HMIENGINE_LIB.HMI_GoBack(66)
