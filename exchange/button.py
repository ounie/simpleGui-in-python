import logging
import queue
import select
import struct
import threading

KEY_OK = 0x160
KEY_LEFT = 105
KEY_RIGHT = 106
KEY_FN_F1 = 0x1d2


class ButtonValue:
    def __init__(self):
        self._value = None
        self.lock = threading.Lock()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self.lock.acquire()
        self._value = value
        self.lock.release()


class InputEvent:
    def __init__(self):
        pass

    def unpack(self, content: bytes):
        # self.time = struct.unpack('8B', content[:8])
        self._type = struct.unpack('H', content[8:10])[0]
        self._code = struct.unpack('H', content[10:12])[0]
        self._value = struct.unpack('I', content[12:])[0]

    @property
    def type(self):
        return self._type

    @property
    def code(self):
        return self._code

    @property
    def value(self):
        return self._value


class ButtonRotaryThread(threading.Thread):
    def __init__(self, input_dev_path, button_event: threading.Event, logger: logging, button: ButtonValue):
        super().__init__()
        self.path = input_dev_path
        file = open(input_dev_path, 'r', encoding='ISO-8859-1')
        self.fds = select.poll()
        self.fd = file.fileno()
        self.fd_obj = file
        self.fds.register(self.fd, select.POLLIN)
        self.input_event = InputEvent()
        self.button_event = button_event
        self.logger = logger
        self.button = button

    def run(self):
        rotary_encoder_queue = []
        while True:
            events = self.fds.poll()
            fd, event = events[0]
            if event & select.POLLIN:
                result = self.fd_obj.read(16).encode('ISO-8859-1')
                self.input_event.unpack(result)
                if self.input_event.type == 0x01:
                    code = self.input_event.code
                    value = self.input_event.value
                    self.button.value = None
                    if code == KEY_OK and value == 1:
                        self.button.value = KEY_OK
                    elif code == KEY_LEFT or code == KEY_RIGHT:
                        rotary_encoder_queue.append(code)
                        if len(rotary_encoder_queue) >= 2:
                            if rotary_encoder_queue[0] > rotary_encoder_queue[1]:
                                self.button.value = KEY_RIGHT
                            elif rotary_encoder_queue[0] < rotary_encoder_queue[1]:
                                self.button.value = KEY_LEFT
                            rotary_encoder_queue.clear()
                    if self.button.value is not None:
                        self.button_event.set()

