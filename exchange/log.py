import logging
import os


class SyncFileHandle(logging.FileHandler):
    def __init__(self, filename, mode='a', encoding=None, delay=False):
        super(SyncFileHandle, self).__init__(filename, mode, encoding, delay)

    def emit(self, record) -> None:
        super(SyncFileHandle, self).emit(record)
        os.sync()


def log_factory(file, level=logging.WARNING, name=None):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    if os.name == 'nt':
        fh = logging.StreamHandler()
    else:
        fh = logging.FileHandler(file)
    formatter = logging.Formatter(fmt='%(asctime)s [%(levelname)s] %(threadName)s %(lineno)s [%(message)s]')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger